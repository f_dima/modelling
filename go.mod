module modelling

go 1.12

require (
	github.com/fsnotify/fsnotify v1.4.9
	github.com/gotk3/gotk3 v0.4.0
	golang.org/x/sys v0.0.0-20200917073148-efd3b9a0ff20 // indirect
)
