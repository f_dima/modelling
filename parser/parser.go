package parser

import (
	"fmt"
	"io/ioutil"
	"regexp"
	"strconv"
	"strings"

	"modelling/gpss"
	"modelling/types"
)

var re = regexp.MustCompile(`\s+`)

//Parser represent report parser
type Parser struct {
	types.Input
	discipline string
}

//Report represent report
type Report struct {
	Ro              float64
	LossesP         []float64
	AverageTimes    []float64
	ArrivedCounts   []int
	ProcessedCounts []int
	AvrgQueueLens   []float64
	StoSize         []int
	AvrgStoCount    []float64
	StoUtil         []float64
	MaxStoCount     []int
	TimeFine        float64
	LossFine        float64
}

//NewParser return new parser object
func NewParser(in types.Input, dis string) *Parser {
	return &Parser{
		Input:      in,
		discipline: dis,
	}
}

//Parse parse report
func (p *Parser) Parse(filePath string) (*Report, error) {
	data, err := ioutil.ReadFile(filePath)

	if err != nil {
		return nil, err
	}

	rep := new(Report)
	var lines, queueLines, lossLines, storageLines, linkLines []string
	var facilityRow string
	var isStart bool
	var losses []int

	for _, line := range strings.Split(string(data), "\n") {
		line = strings.TrimPrefix(line, `\par`)
		line = strings.TrimSpace(line)

		if strings.Contains(line, "GPSS World Simulation Report") {
			isStart = true
		}

		if isStart {
			lines = append(lines, line)
		}
	}

	for i, line := range lines {
		if strings.Contains(line, "QUEUE") && strings.Contains(line, "RETRY") {
			queueLines = lines[i+1 : i+8]
		}

		if strings.Contains(line, "SAVEVALUE") && strings.Contains(line, "RETRY") {
			lossLines = lines[i+1 : i+22]
		}

		if strings.Contains(line, "FACILITY") && strings.Contains(line, "RETRY") {
			facilityRow = lines[i+1]
		}

		if strings.Contains(line, "STORAGE") && strings.Contains(line, "RETRY") {
			storageLines = lines[i+1 : i+7]
		}

		if strings.Contains(line, "USER CHAIN") && strings.Contains(line, "RETRY") &&
			(p.discipline == gpss.AbsolutePriority || p.discipline == gpss.Mixed) {
			for _, myLine := range lines[i+1 : i+7] {
				if values := re.Split(myLine, 7); len(values) < 4 {
					break
				}

				linkLines = append(linkLines, myLine)
			}
		}
	}

	for _, line := range queueLines {
		values := re.Split(line, 9)

		if len(values) == 9 {
			avrgTime, err := strconv.ParseFloat(values[7], 64)

			if err != nil {
				return nil, err
			}

			avrgLen, err := strconv.ParseFloat(values[5], 64)

			if err != nil {
				return nil, err
			}

			rep.AverageTimes = append(rep.AverageTimes, avrgTime)
			rep.AvrgQueueLens = append(rep.AvrgQueueLens, avrgLen)
		} else {
			return nil, fmt.Errorf("block queue: parse error in line `%s`", line)
		}
	}

	for i, line := range lossLines {
		values := re.Split(line, 3)

		if len(values) == 3 {
			count, err := strconv.ParseFloat(values[2], 64)

			if err != nil {
				return nil, err
			}

			if i < 7 {
				losses = append(losses, int(count))
			} else if i >= 7 && i < 14 {
				rep.ArrivedCounts = append(rep.ArrivedCounts, int(count))
			} else {
				rep.ProcessedCounts = append(rep.ProcessedCounts, int(count))
			}
		} else {
			return nil, fmt.Errorf("block savevalue: parse error in line `%s`", line)
		}
	}

	for _, line := range storageLines {
		values := re.Split(line, 11)

		if len(values) == 11 {
			max, err := strconv.Atoi(values[4])

			if err != nil {
				return nil, err
			}

			rep.MaxStoCount = append(rep.MaxStoCount, max)

			size, err := strconv.Atoi(values[1])

			if err != nil {
				return nil, err
			}

			rep.StoSize = append(rep.StoSize, size)

			avg, err := strconv.ParseFloat(values[7], 64)

			if err != nil {
				return nil, err
			}

			rep.AvrgStoCount = append(rep.AvrgStoCount, avg)

			util, err := strconv.ParseFloat(values[8], 64)

			if err != nil {
				return nil, err
			}

			rep.StoUtil = append(rep.StoUtil, util)
		} else {
			return nil, fmt.Errorf("block storage: parse error in line `%s`", line)
		}
	}

	facilityValues := re.Split(facilityRow, 4)

	if len(facilityValues) == 4 {
		rep.Ro, err = strconv.ParseFloat(facilityValues[2], 64)

		if err != nil {
			return nil, err
		}
	} else {
		return nil, fmt.Errorf("block facility: parse error in line `%s`", facilityRow)
	}

	if len(linkLines) > 0 {
		rep.MaxStoCount = make([]int, 6)
		rep.AvrgStoCount = make([]float64, 6)
		rep.StoUtil = make([]float64, 6)
	}

	indexes := []int{}

	for j, v := range rep.StoSize {
		if v != 0 {
			indexes = append(indexes, j)
		}
	}

	for i, line := range linkLines {
		values := re.Split(line, 7)

		if len(values) == 7 {
			max, err := strconv.Atoi(values[5])

			if err != nil {
				return nil, err
			}

			index := indexes[i]
			rep.MaxStoCount[index] = max

			avg, err := strconv.ParseFloat(values[3], 64)

			if err != nil {
				return nil, err
			}

			rep.AvrgStoCount[index] = avg

			util := avg / float64(rep.StoSize[index])
			rep.StoUtil[index] = util
		} else {
			return nil, fmt.Errorf("block user chain: parse error in line `%s`", line)
		}
	}

	var lyambdas []float64

	for _, v := range p.Intervals {
		lyambdas = append(lyambdas, 1/float64(v))
	}

	for i, v := range losses {
		rep.LossesP = append(rep.LossesP, float64(v)/float64(rep.ArrivedCounts[i]))
	}

	for i, v := range lyambdas {
		rep.TimeFine += v * float64(p.TimeFines[i]) * rep.AverageTimes[i]
		rep.LossFine += v * float64(p.LossFines[i]) * rep.LossesP[i]
	}

	return rep, nil
}
