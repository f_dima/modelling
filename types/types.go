package types

// Input represents input parameters
type Input struct {
	Intervals     []int
	Handles       []int
	Switches      []int
	TimeFines     []float64
	LossFines     []float64
	MaxTimeFine   float64
	MaxLossFine   float64
	WorstLosses   float64
	MaxLossP      []float64
	BufferNumbers []int
	Storages      []int
	Priorities    []int
	FirstPr       []int
}
