package gpss

import (
	"bufio"
	"fmt"
	"io"
	"modelling/types"
	"os"
	"sort"
	"strconv"
	"strings"
)

const (
	// RelativePriority is RelativePriority
	RelativePriority = "relative"

	// WithoutPriority is WithoutPriority
	WithoutPriority = "without priority"

	// AbsolutePriority is AbsolutePriority
	AbsolutePriority = "absolute"

	// Mixed is Mixed
	Mixed = "mixed"
)

// Validate validate input
func Validate(input types.Input) error {
	for _, interval := range input.Intervals {
		if interval < 1000 || interval > 1000000 {
			return fmt.Errorf("Средний интервал между поступлением заявок в потоке должен иметь значение от 1000 до 1000000")
		}
	}

	for _, handle := range input.Handles {
		if handle < 500 || handle > 900000 {
			return fmt.Errorf("Среднее время обработки заявок должно иметь значение от 500 до 900000")
		}
	}

	for _, switchT := range input.Switches {
		if switchT < 1 || switchT > 100000 {
			return fmt.Errorf("Время переключения заявок потока должно иметь значение от 1 до 100000")
		}
	}

	for _, timeFine := range input.TimeFines {
		if timeFine < 0 {
			return fmt.Errorf("Штраф за время нахождения заявок в системе должен быть неотрицательным")
		}
	}

	for _, lossFine := range input.LossFines {
		if lossFine < 0 {
			return fmt.Errorf("Штраф за потери заявок в системе должен быть неотрицательным")
		}
	}

	if input.MaxTimeFine < 0 {
		return fmt.Errorf("Суммарный штраф за время нахождения заявок в системе должен быть неотрицательным")
	}

	if input.MaxLossFine < 0 {
		return fmt.Errorf("Суммарный штраф за потери заявок в системе должен быть неотрицательным")
	}

	if input.WorstLosses < 0 || input.WorstLosses > 1 {
		return fmt.Errorf("Вероятность потерь заявок в худшем случае должна иметь значение от 0 до 1")
	}

	for i, p := range input.MaxLossP {
		if p < 0 || p > 1 {
			if i == 6 {
				return fmt.Errorf("Суммарная вероятность потерь заявок во всех потоках должна иметь значение от 0 до 1")
			}

			return fmt.Errorf("Максимальная вероятность потерь заявок должна иметь значение от 0 до 1")
		}
	}

	for _, p := range input.Storages {
		if p <= 0 {
			return fmt.Errorf("Объём буфера должен быть больше 0")
		}
	}

	stors := make(map[int]int)

	for ind, num := range input.BufferNumbers {
		if num < 1 || num > 6 {
			return fmt.Errorf("Номер буфера должен иметь значение от 1 до 6")
		}

		if value, isExists := stors[num]; isExists {
			if input.Storages[ind] != value {
				return fmt.Errorf("Буферы с одинаковыми номерами должны иметь одинаковые объемы")
			}
		}

		stors[num] = input.Storages[ind]
	}

	for _, priority := range input.Priorities {
		if priority < 1 || priority > 6 {
			return fmt.Errorf("Приоритет заявок должен иметь значение от 1 до 6")
		}
	}

	for _, killed := range input.FirstPr {
		if killed < 2 || killed > 7 {
			return fmt.Errorf("Номер первого прерываемого приоритета должен иметь значение от 2 до 7")
		}
	}

	for i := 0; i < 6; i++ {
		if input.Priorities[i] > input.FirstPr[i] {
			return fmt.Errorf("Номер первого прерываемого приоритета должен быть больше приоритета потока")
		}
	}

	return nil
}

// CreateProgram create modelling file
func CreateProgram(typeDiscipline string, input types.Input) error {
	file, err := os.Open(`gpss\GPSS\model.gps`)

	if err != nil {
		return err
	}

	defer file.Close()
	fileContents := bufio.NewReader(file)

	bufNums, storages := processStorages(input.BufferNumbers, input.Storages)
	var slice []byte

	intervals := make([]int, len(input.Intervals))
	copy(intervals, input.Intervals)
	sort.Ints(intervals)
	modellingTime := intervals[0] * 10000

	var (
		flIsAbsPr, pr1, pr2, pr3, pr4, pr5, pr6,
		flInt1, flInt2, flInt3, flInt4, flInt5, flInt6,
		flHandle1, flHandle2, flHandle3, flHandle4, flHandle5, flHandle6,
		flSwitch1, flSwitch2, flSwitch3, flSwitch4, flSwitch5, flSwitch6,
		flFirstPr1, flFirstPr2, flFirstPr3, flFirstPr4, flFirstPr5, flFirstPr6 bool
	)

	for {
		line, err := fileContents.ReadBytes('\n')

		if err != nil {
			if err == io.EOF {
				break
			}

			return err
		}

		if strings.Contains(string(line), "GENERATE") && !strings.Contains(string(line), "Exponential") {
			length := len([]byte("100000000 \n"))
			line = line[:len(line)-length]
			num := fmt.Sprintf("%d \n", modellingTime)
			line = append(line, []byte(num)...)
		}

		if strings.Contains(string(line), "IS_ABS_PR") && !flIsAbsPr {
			if typeDiscipline == AbsolutePriority || typeDiscipline == Mixed {
				length := len([]byte("0 \n"))
				line = line[:len(line)-length]
				line = append(line, []byte("1 \n")...)
			}

			flIsAbsPr = true
		}

		line, pr1 = parseIntValue(line, 7-input.Priorities[0], "POTOK_PR1", "1 \n", pr1)
		line, pr2 = parseIntValue(line, 7-input.Priorities[1], "POTOK_PR2", "1 \n", pr2)
		line, pr3 = parseIntValue(line, 7-input.Priorities[2], "POTOK_PR3", "1 \n", pr3)
		line, pr4 = parseIntValue(line, 7-input.Priorities[3], "POTOK_PR4", "1 \n", pr4)
		line, pr5 = parseIntValue(line, 7-input.Priorities[4], "POTOK_PR5", "1 \n", pr5)
		line, pr6 = parseIntValue(line, 7-input.Priorities[5], "POTOK_PR6", "1 \n", pr6)

		line, flInt1 = parseIntValue(line, input.Intervals[0], "INTERVAL1", "100 \n", flInt1)
		line, flInt2 = parseIntValue(line, input.Intervals[1], "INTERVAL2", "100 \n", flInt2)
		line, flInt3 = parseIntValue(line, input.Intervals[2], "INTERVAL3", "100 \n", flInt3)
		line, flInt4 = parseIntValue(line, input.Intervals[3], "INTERVAL4", "100 \n", flInt4)
		line, flInt5 = parseIntValue(line, input.Intervals[4], "INTERVAL5", "100 \n", flInt5)
		line, flInt6 = parseIntValue(line, input.Intervals[5], "INTERVAL6", "100 \n", flInt6)

		line, flHandle1 = parseIntValue(line, input.Handles[0], "HANDLE1", "1 \n", flHandle1)
		line, flHandle2 = parseIntValue(line, input.Handles[1], "HANDLE2", "1 \n", flHandle2)
		line, flHandle3 = parseIntValue(line, input.Handles[2], "HANDLE3", "1 \n", flHandle3)
		line, flHandle4 = parseIntValue(line, input.Handles[3], "HANDLE4", "1 \n", flHandle4)
		line, flHandle5 = parseIntValue(line, input.Handles[4], "HANDLE5", "1 \n", flHandle5)
		line, flHandle6 = parseIntValue(line, input.Handles[5], "HANDLE6", "1 \n", flHandle6)

		line, flSwitch1 = parseIntValue(line, input.Switches[0], "SWITCH1", "1 \n", flSwitch1)
		line, flSwitch2 = parseIntValue(line, input.Switches[1], "SWITCH2", "1 \n", flSwitch2)
		line, flSwitch3 = parseIntValue(line, input.Switches[2], "SWITCH3", "1 \n", flSwitch3)
		line, flSwitch4 = parseIntValue(line, input.Switches[3], "SWITCH4", "1 \n", flSwitch4)
		line, flSwitch5 = parseIntValue(line, input.Switches[4], "SWITCH5", "1 \n", flSwitch5)
		line, flSwitch6 = parseIntValue(line, input.Switches[5], "SWITCH6", "1 \n", flSwitch6)

		line, flFirstPr1 = parseIntValue(line, 7-input.FirstPr[0], "FIRSTPR1", "1 \n", flFirstPr1)
		line, flFirstPr2 = parseIntValue(line, 7-input.FirstPr[1], "FIRSTPR2", "1 \n", flFirstPr2)
		line, flFirstPr3 = parseIntValue(line, 7-input.FirstPr[2], "FIRSTPR3", "1 \n", flFirstPr3)
		line, flFirstPr4 = parseIntValue(line, 7-input.FirstPr[3], "FIRSTPR4", "1 \n", flFirstPr4)
		line, flFirstPr5 = parseIntValue(line, 7-input.FirstPr[4], "FIRSTPR5", "1 \n", flFirstPr5)
		line, flFirstPr6 = parseIntValue(line, 7-input.FirstPr[5], "FIRSTPR6", "1 \n", flFirstPr6)

		line = parseBuffers(line, "STO1", "STORAGE", storages[0])
		line = parseBuffers(line, "STO2", "STORAGE", storages[1])
		line = parseBuffers(line, "STO3", "STORAGE", storages[2])
		line = parseBuffers(line, "STO4", "STORAGE", storages[3])
		line = parseBuffers(line, "STO5", "STORAGE", storages[4])
		line = parseBuffers(line, "STO6", "STORAGE", storages[5])

		line = parseBuffers(line, "BUFNUM1", "EQU", bufNums[0])
		line = parseBuffers(line, "BUFNUM2", "EQU", bufNums[1])
		line = parseBuffers(line, "BUFNUM3", "EQU", bufNums[2])
		line = parseBuffers(line, "BUFNUM4", "EQU", bufNums[3])
		line = parseBuffers(line, "BUFNUM5", "EQU", bufNums[4])
		line = parseBuffers(line, "BUFNUM6", "EQU", bufNums[5])

		slice = append(slice, line...)
	}

	writeFile, err := os.Create(`gpss\GPSS\new.gps`)

	if err != nil {
		return err
	}

	defer writeFile.Close()
	_, err = writeFile.Write(slice)

	if err != nil {
		return err
	}

	return nil
}

func parseIntValue(line []byte, value int, field string, existValue string, fl bool) ([]byte, bool) {
	if strings.Contains(string(line), field) && !fl {
		length := len([]byte(existValue))
		line = line[:len(line)-length]
		line = append(line, []byte(strconv.Itoa(value)+" \n")...)
		fl = true
	}

	return line, fl
}

func parseBuffers(line []byte, str1 string, str2 string, value int) []byte {
	if strings.Contains(string(line), str1) && strings.Contains(string(line), str2) {
		length := len([]byte("1 \n"))
		line = line[:len(line)-length]
		line = append(line, []byte(strconv.Itoa(value)+" \n")...)
	}

	return line
}

func processStorages(bufNums, stoCaps []int) (outBufNums, outStoCaps []int) {
	numToSize := make(map[int]int)

	for i, v := range bufNums {
		numToSize[v] = stoCaps[i]
		outBufNums = append(outBufNums, i+1)
	}

	outStoCaps = make([]int, len(outBufNums))

	for i, v := range outBufNums {
		if size, ok := numToSize[v]; ok {
			outStoCaps[i] = size
		}
	}

	return bufNums, outStoCaps
}
