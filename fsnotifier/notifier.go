package fsnotifier

import (
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/fsnotify/fsnotify"
)

const dirPath = `gpss\GPSS\`

// Run launch file watcher
func Run(fileNames chan string, done chan struct{}) {
	watcher, err := fsnotify.NewWatcher()

	if err != nil {
		handleError(err)
	}

	defer watcher.Close()

	go func() {
		for {
			select {
			case event, ok := <-watcher.Events:
				if !ok {
					return
				}

				if len(event.Name) != 0 {
					log.Println("event:", event)
				}

				if event.Op&fsnotify.Write == fsnotify.Write {
					if strings.HasSuffix(event.Name, ".gpr") {
						fileNames <- event.Name
					}
				}

				if event.Op&fsnotify.Remove == fsnotify.Remove {
					log.Println("file removed: ", event.Name)
				}
			case err, ok := <-watcher.Errors:
				if !ok {
					return
				}

				log.Println("fsnotifier error: ", err)
			}
		}
	}()

	err = watcher.Add(dirPath)

	if err != nil {
		handleError(err)
	}

	<-done
}

func handleError(err interface{}) {
	log.Println("Ошибка: ", err)
	log.Println("Произошла ошибка. Для выхода из программы введите ENTER в этом окне")
	fmt.Scanln()
	os.Exit(1)
}
