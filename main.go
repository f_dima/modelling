package main

import (
	"fmt"
	"log"
	"modelling/fsnotifier"
	"modelling/gpss"
	"modelling/parser"
	"modelling/types"
	"os"
	"os/exec"
	"strconv"

	"github.com/gotk3/gotk3/gtk"
)

func main() {
	log.SetFlags(0)
	gtk.Init(nil)

	b, err := gtk.BuilderNew()

	if err != nil {
		handleError(err)
	}

	err = b.AddFromFile("main.glade")

	if err != nil {
		handleError(err)
	}

	mainWindowObj, err := b.GetObject("window2")

	if err != nil {
		handleError(err)
	}

	mainWindow := mainWindowObj.(*gtk.Window)
	_, err = mainWindow.Connect("destroy", func() {
		gtk.MainQuit()
	})

	if err != nil {
		handleError(err)
	}

	modellingWindowObj, err := b.GetObject("win")

	if err != nil {
		handleError(err)
	}

	modellingWindow := modellingWindowObj.(*gtk.Window)
	_, err = modellingWindow.Connect("destroy", func() {
		gtk.MainQuit()
	})

	if err != nil {
		handleError(err)
	}

	nextButtonObj, err := b.GetObject("button_next")

	if err != nil {
		handleError(err)
	}

	nextButton := nextButtonObj.(*gtk.Button)
	_, err = nextButton.Connect("clicked", func() {
		modellingWindow.ShowAll()
		mainWindow.Hide()
	})

	if err != nil {
		handleError(err)
	}

	buttonBackObj, err := b.GetObject("button_back")

	if err != nil {
		handleError(err)
	}

	buttonBack := buttonBackObj.(*gtk.Button)
	_, err = buttonBack.Connect("clicked", func() {
		mainWindow.ShowAll()
		modellingWindow.Hide()
	})

	if err != nil {
		handleError(err)
	}

	prioritiesTableObj, err := b.GetObject("grid9")

	if err != nil {
		handleError(err)
	}

	prioritiesTable := prioritiesTableObj.(*gtk.Grid)

	mixPrioritiesTableObj, err := b.GetObject("box5")

	if err != nil {
		handleError(err)
	}

	mixPrioritiesTable := mixPrioritiesTableObj.(*gtk.Box)

	noPrioritiesObj, err := b.GetObject("radiobutton7")

	if err != nil {
		handleError(err)
	}

	noPriorities := noPrioritiesObj.(*gtk.RadioButton)
	_, err = noPriorities.Connect("clicked", func() {
		prioritiesTable.SetVisible(false)
		mixPrioritiesTable.SetVisible(false)
	})

	if err != nil {
		handleError(err)
	}

	relPrioritiesObj, err := b.GetObject("radiobutton8")

	if err != nil {
		handleError(err)
	}

	relPriorities := relPrioritiesObj.(*gtk.RadioButton)
	_, err = relPriorities.Connect("clicked", func() {
		prioritiesTable.SetVisible(true)
		mixPrioritiesTable.SetVisible(false)
	})

	if err != nil {
		handleError(err)
	}

	absPrioritiesObj, err := b.GetObject("radiobutton9")

	if err != nil {
		handleError(err)
	}

	absPriorities := absPrioritiesObj.(*gtk.RadioButton)
	_, err = absPriorities.Connect("clicked", func() {
		prioritiesTable.SetVisible(true)
		mixPrioritiesTable.SetVisible(false)
	})

	if err != nil {
		handleError(err)
	}

	mixPrioritiesObj, err := b.GetObject("radiobutton12")

	if err != nil {
		handleError(err)
	}

	mixPriorities := mixPrioritiesObj.(*gtk.RadioButton)
	_, err = mixPriorities.Connect("clicked", func() {
		prioritiesTable.SetVisible(true)
		mixPrioritiesTable.SetVisible(true)
	})

	if err != nil {
		handleError(err)
	}

	comBufferTableObj, err := b.GetObject("grid10")

	if err != nil {
		handleError(err)
	}

	comBufferTable := comBufferTableObj.(*gtk.Grid)

	buffersTableObj, err := b.GetObject("grid11")

	if err != nil {
		handleError(err)
	}

	buffersTable := buffersTableObj.(*gtk.Grid)

	comBufferObj, err := b.GetObject("radiobutton10")

	if err != nil {
		handleError(err)
	}

	comBuffer := comBufferObj.(*gtk.RadioButton)
	_, err = comBuffer.Connect("clicked", func() {
		buffersTable.SetVisible(false)
		comBufferTable.SetVisible(true)
	})

	if err != nil {
		handleError(err)
	}

	splitBufferObj, err := b.GetObject("radiobutton11")

	if err != nil {
		handleError(err)
	}

	splitBuffer := splitBufferObj.(*gtk.RadioButton)
	_, err = splitBuffer.Connect("clicked", func() {
		buffersTable.SetVisible(true)
		comBufferTable.SetVisible(false)
	})

	if err != nil {
		handleError(err)
	}

	initEntries(b)
	helpMenuObj, err := b.GetObject("help")

	if err != nil {
		handleError(err)
	}

	helpMenu := helpMenuObj.(*gtk.MenuItem)
	_, err = helpMenu.Connect("activate", func() {
		dialogObj, err := b.GetObject("dialog-about-program")

		if err != nil {
			handleError(err)
		}

		dialog := dialogObj.(*gtk.Dialog)
		closeObj, err := b.GetObject("button-ok-about-program")

		if err != nil {
			handleError(err)
		}

		closeButton := closeObj.(*gtk.Button)
		_, err = closeButton.Connect("clicked", func() {
			dialog.Close()
			dialog.HideOnDelete()
		})

		if err != nil {
			handleError(err)
		}

		dialog.HideOnDelete()
		dialog.Run()
	})

	if err != nil {
		handleError(err)
	}

	kickNotifier := make(chan struct{})
	fileNames := make(chan string, 1000)

	go fsnotifier.Run(fileNames, kickNotifier)
	defer func() { kickNotifier <- struct{}{} }()

	startObj, err := b.GetObject("button_start")

	if err != nil {
		handleError(err)
	}

	start := startObj.(*gtk.Button)
	_, err = start.Connect("clicked", func() {
		input, discipline, errMsg := initInputValues(b,
			noPriorities, relPriorities, absPriorities, mixPriorities, comBuffer, splitBuffer)

		if errMsg != "" {
			errorDialogHandler(b, errMsg)
			return
		}

		err = gpss.Validate(input)

		if err != nil {
			errorDialogHandler(b, err.Error())
			return
		}

		err = gpss.CreateProgram(discipline, input)

		if err != nil {
			handleError(err)
		}

		cmd := exec.Command(`gpss\GPSS\GPSSW`, `gpss\GPSS\new.gps`, "BATCH")
		cmd.Stdout = os.Stdout
		cmd.Stdin = os.Stdin
		cmd.Stderr = os.Stderr

		err = cmd.Run()

		if err != nil {
			handleError(err)
		}

		repName := `gpss\GPSS\new.58.1.gpr`

		for i := 0; i < len(fileNames); i++ {
			name := <-fileNames
			repName = name
		}

		parser := parser.NewParser(input, discipline)
		report, err := parser.Parse(repName)

		if err != nil {
			handleError(err)
		}

		err = openResultWindow(b, *report, input)

		if err != nil {
			handleError(err)
		}
	})

	if err != nil {
		handleError(err)
	}

	exitObj, err := b.GetObject("file_exit")

	if err != nil {
		handleError(err)
	}

	exit := exitObj.(*gtk.MenuItem)
	_, err = exit.Connect("activate", func() {
		gtk.MainQuit()
	})

	if err != nil {
		handleError(err)
	}

	mainWindow.ShowAll()
	gtk.Main()
}

func initInputValues(b *gtk.Builder, noPri, relPri, absPri, mixPri, comBuf, splitBuf *gtk.RadioButton) (types.Input, string, string) {
	var input types.Input
	var errorMsg, discipline string
	var err error

	input.Intervals, err = getIntEntries("1", "Средний интервал между поступлениями заявок", b)

	if err != nil {
		errorMsg += err.Error()
	}

	input.Handles, err = getIntEntries("2", "Среднее время обработки заявок", b)

	if err != nil {
		errorMsg += err.Error()
	}

	input.Switches, err = getIntEntries("3", "Время переключения заявок потока", b)

	if err != nil {
		errorMsg += err.Error()
	}

	input.TimeFines, err = getFloatEntries("4", "Штраф за нахождение заявок в системе", b)

	if err != nil {
		errorMsg += err.Error()
	}

	input.LossFines, err = getFloatEntries("5", "Штраф за потери заявок", b)

	if err != nil {
		errorMsg += err.Error()
	}

	input.MaxLossP, err = getFloatEntries("6", "Максимальная вероятность потерь заявок", b)

	if err != nil {
		errorMsg += err.Error()
	}

	sumLossesObj, err := b.GetObject("entry_sum_losses")

	if err != nil {
		handleError(err)
	}

	sumLossesEntry := sumLossesObj.(*gtk.Entry)
	sumLossesStr, err := sumLossesEntry.GetText()

	if err != nil {
		handleError(err)
	}

	sumLosses, err := strconv.ParseFloat(sumLossesStr, 64)

	if err != nil {
		errorMsg += fmt.Sprintf("Неверный тип суммарной вероятности потерь заявок во всех потоках\n")
	}

	input.MaxLossP = append(input.MaxLossP, sumLosses)

	switch {
	case comBuf.GetActive():
		input.BufferNumbers = []int{1, 1, 1, 1, 1, 1}
		comBufSizeObj, err := b.GetObject("entry_size")

		if err != nil {
			handleError(err)
		}

		comBufSize := comBufSizeObj.(*gtk.Entry)
		sizeStr, err := comBufSize.GetText()

		if err != nil {
			handleError(err)
		}

		size, err := strconv.Atoi(sizeStr)

		if err != nil {
			errorMsg += fmt.Sprintf("Неверный тип размера общего буфера\n")
		}

		input.Storages = []int{size, size, size, size, size, size}
	case splitBuf.GetActive():
		input.BufferNumbers, err = getIntEntries("9", "Номер буфера", b)

		if err != nil {
			errorMsg += err.Error()
		}

		input.Storages, err = getIntEntries("10", "Объём буфера", b)

		if err != nil {
			errorMsg += err.Error()
		}
	}

	switch {
	case noPri.GetActive():
		discipline = gpss.WithoutPriority
		input.Priorities = []int{1, 1, 1, 1, 1, 1}
		input.FirstPr = []int{2, 2, 2, 2, 2, 2}
	case relPri.GetActive():
		discipline = gpss.RelativePriority
		input.Priorities, err = getIntEntries("7", "Приоритет заявок", b)

		if err != nil {
			errorMsg += err.Error()
		}

		input.FirstPr = []int{7, 7, 7, 7, 7, 7}
	case absPri.GetActive():
		discipline = gpss.AbsolutePriority
		input.Priorities, err = getIntEntries("7", "Приоритет заявок", b)

		if err != nil {
			errorMsg += err.Error()
		}

		input.FirstPr = make([]int, 0, len(input.Priorities))

		for _, v := range input.Priorities {
			input.FirstPr = append(input.FirstPr, v+1)
		}
	case mixPri.GetActive():
		discipline = gpss.Mixed
		input.Priorities, err = getIntEntries("7", "Приоритет заявок", b)

		if err != nil {
			errorMsg += err.Error()
		}

		input.FirstPr, err = getIntEntries("8", "Номер первого прерываемого приоритета", b)

		if err != nil {
			errorMsg += err.Error()
		}
	}

	timeFineObj, err := b.GetObject("entry_time_fine")

	if err != nil {
		handleError(err)
	}

	timeFineEntry := timeFineObj.(*gtk.Entry)
	timeFineStr, err := timeFineEntry.GetText()

	if err != nil {
		handleError(err)
	}

	timeFine, err := strconv.ParseFloat(timeFineStr, 64)

	if err != nil {
		errorMsg += fmt.Sprintf("Неверный тип максимального штрафа за время нахождения заявок в системе\n")
	}

	input.MaxTimeFine = timeFine

	lossFineObj, err := b.GetObject("entry_loss_fine")

	if err != nil {
		handleError(err)
	}

	lossFineEntry := lossFineObj.(*gtk.Entry)
	lossFineStr, err := lossFineEntry.GetText()

	if err != nil {
		handleError(err)
	}

	lossFine, err := strconv.ParseFloat(lossFineStr, 64)

	if err != nil {
		errorMsg += fmt.Sprintf("Неверный тип максимального штрафа за потери заявок в системе\n")
	}

	input.MaxLossFine = lossFine

	worstLossesObj, err := b.GetObject("entry_worst_losses")

	if err != nil {
		handleError(err)
	}

	worstLossesEntry := worstLossesObj.(*gtk.Entry)
	worstLossesStr, err := worstLossesEntry.GetText()

	if err != nil {
		handleError(err)
	}

	worstLosses, err := strconv.ParseFloat(worstLossesStr, 64)

	if err != nil {
		errorMsg += fmt.Sprintf("Неверный тип вероятности потерь потери заявок в худшем случае\n")
	}

	input.WorstLosses = worstLosses

	return input, discipline, errorMsg
}

func getIntEntries(columnNumber, columnName string, b *gtk.Builder) ([]int, error) {
	var values []int
	var errorMessage string

	for i := 1; i < 7; i++ {
		labelName := "entry_" + strconv.Itoa(i) + "_" + columnNumber
		obj, err := b.GetObject(labelName)

		if err != nil {
			handleError(err)
		}

		entry := obj.(*gtk.Entry)
		ent, err := entry.GetText()

		if err != nil {
			handleError(err)
		}

		val, err := strconv.Atoi(ent)

		if err != nil {
			errorMessage += fmt.Sprintf("Неверный тип: строка %d в столбце %q\n", i, columnName)
		}

		values = append(values, val)
	}

	if errorMessage != "" {
		return nil, fmt.Errorf("%s", errorMessage)
	}

	return values, nil
}

func getFloatEntries(columnNumber, columnName string, b *gtk.Builder) ([]float64, error) {
	var values []float64
	var errorMessage string

	for i := 1; i < 7; i++ {
		labelName := "entry_" + strconv.Itoa(i) + "_" + columnNumber
		obj, err := b.GetObject(labelName)

		if err != nil {
			handleError(err)
		}

		entry := obj.(*gtk.Entry)
		ent, err := entry.GetText()

		if err != nil {
			handleError(err)
		}

		val, err := strconv.ParseFloat(ent, 64)

		if err != nil {
			errorMessage += fmt.Sprintf("Неверный тип: строка %d в столбце %q\n", i, columnName)
		}

		values = append(values, val)
	}

	if errorMessage != "" {
		return nil, fmt.Errorf("%s", errorMessage)
	}

	return values, nil
}

func errorDialogHandler(b *gtk.Builder, errorMsg string) {
	win, err := b.GetObject("dialog-error")

	if err != nil {
		handleError(err)
	}

	errorWin := win.(*gtk.Dialog)
	buttonObj, err := b.GetObject("button-ok-error")

	if err != nil {
		handleError(err)
	}

	buttonOk := buttonObj.(*gtk.Button)
	_, err = buttonOk.Connect("clicked", func() {
		errorWin.Close()
		errorWin.HideOnDelete()
	})

	if err != nil {
		handleError(err)
	}

	labelObj, err := b.GetObject("label-error")

	if err != nil {
		handleError(err)
	}

	label := labelObj.(*gtk.Label)
	label.SetText(errorMsg)

	errorWin.HideOnDelete()
	errorWin.Run()
}

func initEntries(b *gtk.Builder) {
	for i := 1; i < 7; i++ {
		obj, err := b.GetObject("entry_" + strconv.Itoa(i) + "_1")

		if err != nil {
			handleError(err)
		}

		entry := obj.(*gtk.Entry)
		entry.SetText("10000")

		obj, err = b.GetObject("entry_" + strconv.Itoa(i) + "_2")

		if err != nil {
			handleError(err)
		}

		entry = obj.(*gtk.Entry)
		entry.SetText("1000")

		obj, err = b.GetObject("entry_" + strconv.Itoa(i) + "_3")

		if err != nil {
			handleError(err)
		}

		entry = obj.(*gtk.Entry)
		entry.SetText("1")

		obj, err = b.GetObject("entry_" + strconv.Itoa(i) + "_4")

		if err != nil {
			handleError(err)
		}

		entry = obj.(*gtk.Entry)
		entry.SetText("10")

		obj, err = b.GetObject("entry_" + strconv.Itoa(i) + "_5")

		if err != nil {
			handleError(err)
		}

		entry = obj.(*gtk.Entry)
		entry.SetText("10")

		obj, err = b.GetObject("entry_" + strconv.Itoa(i) + "_6")

		if err != nil {
			handleError(err)
		}

		entry = obj.(*gtk.Entry)
		entry.SetText("0.1")

		obj, err = b.GetObject("entry_" + strconv.Itoa(i) + "_7")

		if err != nil {
			handleError(err)
		}

		entry = obj.(*gtk.Entry)
		entry.SetText(fmt.Sprintf("%d", i))

		obj, err = b.GetObject("entry_" + strconv.Itoa(i) + "_8")

		if err != nil {
			handleError(err)
		}

		entry = obj.(*gtk.Entry)
		entry.SetText(fmt.Sprintf("%d", i+1))

		obj, err = b.GetObject("entry_" + strconv.Itoa(i) + "_9")

		if err != nil {
			handleError(err)
		}

		entry = obj.(*gtk.Entry)
		entry.SetText(fmt.Sprintf("%d", i))

		obj, err = b.GetObject("entry_" + strconv.Itoa(i) + "_10")

		if err != nil {
			handleError(err)
		}

		entry = obj.(*gtk.Entry)
		entry.SetText(fmt.Sprintf("%d", i))
	}

	obj, err := b.GetObject("entry_size")

	if err != nil {
		handleError(err)
	}

	entry := obj.(*gtk.Entry)
	entry.SetText("1000")

	obj, err = b.GetObject("entry_time_fine")

	if err != nil {
		handleError(err)
	}

	entry = obj.(*gtk.Entry)
	entry.SetText("10")

	obj, err = b.GetObject("entry_loss_fine")

	if err != nil {
		handleError(err)
	}

	entry = obj.(*gtk.Entry)
	entry.SetText("10")

	obj, err = b.GetObject("entry_worst_losses")

	if err != nil {
		handleError(err)
	}

	entry = obj.(*gtk.Entry)
	entry.SetText("0.1")

	obj, err = b.GetObject("entry_sum_losses")

	if err != nil {
		handleError(err)
	}

	entry = obj.(*gtk.Entry)
	entry.SetText("0.1")
}

func openResultWindow(b *gtk.Builder, rep parser.Report, input types.Input) error {
	obj, err := b.GetObject("window1")

	if err != nil {
		return err
	}

	win := obj.(*gtk.Window)

	objRo, err := b.GetObject("ro")

	if err != nil {
		return err
	}

	roLabel := objRo.(*gtk.Label)

	roLabel.SetText(strconv.FormatFloat(rep.Ro, 'f', 3, 64))

	tFineObj, err := b.GetObject("timeFine")

	if err != nil {
		return err
	}

	timeFine := tFineObj.(*gtk.Label)
	repFine := strconv.FormatFloat(rep.TimeFine, 'f', 7, 64)
	inputFine := strconv.FormatFloat(input.MaxTimeFine, 'f', 7, 64)

	rep.TimeFine, _ = strconv.ParseFloat(repFine, 64)
	input.MaxTimeFine, _ = strconv.ParseFloat(inputFine, 64)

	if rep.TimeFine > input.MaxTimeFine {
		timeFine.SetMarkup(fmt.Sprintf(`<span foreground="red">%s</span> (> %s)`, repFine, inputFine))
	} else {
		timeFine.SetText(fmt.Sprintf("%s (<= %s)", repFine, inputFine))
	}

	lFineObj, err := b.GetObject("lossFine")

	if err != nil {
		return err
	}

	lossFine := lFineObj.(*gtk.Label)
	repFine = strconv.FormatFloat(rep.LossFine, 'f', 7, 64)
	inputFine = strconv.FormatFloat(input.MaxLossFine, 'f', 7, 64)

	rep.LossFine, _ = strconv.ParseFloat(repFine, 64)
	input.MaxLossFine, _ = strconv.ParseFloat(inputFine, 64)

	if rep.LossFine > input.MaxLossFine {
		lossFine.SetMarkup(fmt.Sprintf(`<span foreground="red">%s</span> (> %s)`, repFine, inputFine))
	} else {
		lossFine.SetText(fmt.Sprintf("%s (<= %s)", repFine, inputFine))
	}

	worstLossesObj, err := b.GetObject("worst_losses")

	if err != nil {
		return err
	}

	worstLosses := worstLossesObj.(*gtk.Label)
	worstMaxLosses := rep.LossesP[0]

	for _, v := range rep.LossesP {
		if v > worstMaxLosses {
			worstMaxLosses = v
		}
	}

	repFine = strconv.FormatFloat(worstMaxLosses, 'f', 7, 64)
	inputFine = strconv.FormatFloat(input.WorstLosses, 'f', 7, 64)

	worstMaxLosses, _ = strconv.ParseFloat(repFine, 64)
	input.WorstLosses, _ = strconv.ParseFloat(inputFine, 64)

	if worstMaxLosses > input.WorstLosses {
		worstLosses.SetMarkup(fmt.Sprintf(`<span foreground="red">%s</span> (> %s)`, repFine, inputFine))
	} else {
		worstLosses.SetText(fmt.Sprintf("%s (<= %s)", repFine, inputFine))
	}

	for ind, value := range rep.LossesP {
		obj, err := b.GetObject("lossesP" + strconv.Itoa(ind+1))

		if err != nil {
			return err
		}

		label := obj.(*gtk.Label)
		repFine = strconv.FormatFloat(value, 'f', 7, 64)
		inputFine = strconv.FormatFloat(input.MaxLossP[ind], 'f', 7, 64)

		value, _ = strconv.ParseFloat(repFine, 64)
		input.MaxLossP[ind], _ = strconv.ParseFloat(inputFine, 64)

		if value > input.MaxLossP[ind] {
			label.SetMarkup(fmt.Sprintf(`<span foreground="red">%s</span> (> %s)`, repFine, inputFine))
		} else {
			label.SetText(fmt.Sprintf("%s (<= %s)", repFine, inputFine))
		}
	}

	for ind, value := range rep.ArrivedCounts {
		obj, err := b.GetObject("arrivedCounts" + strconv.Itoa(ind+1))

		if err != nil {
			return err
		}

		label := obj.(*gtk.Label)
		label.SetText(strconv.Itoa(value))
	}

	for ind, value := range rep.AverageTimes {
		obj, err := b.GetObject("averageTimes" + strconv.Itoa(ind+1))

		if err != nil {
			return err
		}

		label := obj.(*gtk.Label)
		label.SetText(strconv.FormatFloat(value, 'f', 3, 64))
	}

	for ind, value := range rep.AvrgQueueLens {
		obj, err := b.GetObject("avrgQueueLens" + strconv.Itoa(ind+1))

		if err != nil {
			return err
		}

		label := obj.(*gtk.Label)
		label.SetText(strconv.FormatFloat(value, 'f', 3, 64))
	}

	for ind, value := range rep.ProcessedCounts {
		obj, err := b.GetObject("processedCounts" + strconv.Itoa(ind+1))

		if err != nil {
			return err
		}

		label := obj.(*gtk.Label)
		label.SetText(strconv.Itoa(value))
	}

	for ind, value := range rep.StoSize {
		obj, err := b.GetObject("stoSize" + strconv.Itoa(ind+1))

		if err != nil {
			return err
		}

		label := obj.(*gtk.Label)
		label.SetText(strconv.Itoa(value))
	}

	for ind, value := range rep.MaxStoCount {
		obj, err := b.GetObject("maxStoCount" + strconv.Itoa(ind+1))

		if err != nil {
			return err
		}

		label := obj.(*gtk.Label)
		label.SetText(strconv.Itoa(value))
	}

	for ind, value := range rep.AvrgStoCount {
		obj, err := b.GetObject("avrgStoCount" + strconv.Itoa(ind+1))

		if err != nil {
			return err
		}

		label := obj.(*gtk.Label)
		label.SetText(strconv.FormatFloat(value, 'f', 3, 64))
	}

	for ind, value := range rep.StoUtil {
		obj, err := b.GetObject("stoUtil" + strconv.Itoa(ind+1))

		if err != nil {
			return err
		}

		label := obj.(*gtk.Label)
		label.SetText(strconv.FormatFloat(value, 'f', 3, 64))
	}

	win.HideOnDelete()
	win.Present()
	win.ShowAll()
	return nil
}

func handleError(err interface{}) {
	log.Println("Ошибка: ", err)
	log.Println("Произошла ошибка. Для выхода из программы введите ENTER в этом окне")
	fmt.Scanln()
	os.Exit(1)
}
